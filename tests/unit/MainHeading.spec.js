import { shallowMount } from "@vue/test-utils";

import MainHeading from "@/components/Home/MainHeading.vue";

describe("MainHeading.vue", () => {
  it("matches snapshot", () => {
    const wrapper = shallowMount(MainHeading);
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders props.message as the main heading", () => {
    const message = "heading";
    const wrapper = shallowMount(MainHeading, {
      propsData: { message }
    });
    expect(wrapper.text()).toMatch(message);
  });
});
