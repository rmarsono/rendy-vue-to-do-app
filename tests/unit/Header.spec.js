import { shallowMount } from "@vue/test-utils";

import Header from "@/components/Home/Header.vue";

describe("Header.vue", () => {
  it("matches snapshot", () => {
    const wrapper = shallowMount(Header);
    expect(wrapper.html()).toMatchSnapshot();
  });
});
