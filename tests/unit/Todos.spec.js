import Vuex from "vuex";
import { mount, createLocalVue } from "@vue/test-utils";

import Todos from "@/components/Home/Todos.vue";

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
  state: {
    todos: [
      {
        userId: 1,
        id: 1,
        title: "delectus aut autem",
        completed: false
      },
      {
        userId: 1,
        id: 2,
        title: "quis ut nam facilis et officia qui",
        completed: false
      },
      {
        userId: 1,
        id: 3,
        title: "fugiat veniam minus",
        completed: false
      }
    ],
    isLoading: false,
    currentUser: 1
  },
  getters: {
    todos: state => state.todos,
    isLoading: state => state.isLoading,
    currentUser: state => state.currentUser
  }
});

describe("Todos.vue", () => {
  it("renders 3 todos", () => {
    const wrapper = mount(Todos, {
      store,
      localVue
    });
    const todos = wrapper.findAll(".todo-wrap");
    expect(todos.length).toBe(3);
  });
});
