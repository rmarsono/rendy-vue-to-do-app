import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";

import UserSelector from "@/components/Home/UserSelector.vue";

const localVue = createLocalVue();
localVue.use(Vuex);

describe("UserSelector.vue", () => {
  let store;
  let getters;

  beforeEach(() => {
    getters = {
      users: () => [1, 2, 3, 4, 5],
      isLoading: () => false,
      currentUser: () => 1
    };
    store = new Vuex.Store({ getters });
  });

  it("renders 5 users", () => {
    const wrapper = shallowMount(UserSelector, {
      store,
      localVue
    });
    const users = wrapper.findAll(".user-selector__option");
    expect(users.length).toBe(5);
  });
});
