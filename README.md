# Rendy's To-do app for Valiant

As per the brief, I am consuming this API: https://jsonplaceholder.typicode.com/todos

## Notes & assumptions

I interpret the API data as the following:
1. The to-do items are organised based on users; there are multiple users and each user can have multiple to-do items associated with them.
1. The current user can be switched via a select list on the top right corner of the app; changing the current user will re-populate the to-do list to match that user.
1. Within the list, users can mark a to-do item as complete, modify the title and delete items.
1. At the bottom of the page, users can add new to-do items to the list for the currently selected User.

## How to review

in the **master** branch, run **npm run serve** and navigate to the link provided to review the app. To run the unit tests, run **npm run test:unit**

## Development log

### Tuesday 16 July 2019 9:25pm

I tried going through a Vue tutorial as well as read an article on learning Vue as a React eeveloper but nothing beats just diving in a trying things out. I installed the Vue CLI, Vetur for my VS Code and stated coding.

I wanted to create a simple Hello World app but I decided to just create the To-do app straight away.

At the start, I found it a bit difficult to follow the syntax and I have to continually refer to the Vue.js website to see what I'm doing wrong. However, in the end I am glad that I didn't waste time watching tutorial videos/reading documents because Vetur is really helpful in setting up the boilerplate. The CLI is also cool because it handles all the Vuex setup out of the box.

### Wednesday 17 July 2019 11:48pm

I managed to sort out the **UserSelector** issue; I was trying to set the value of the **currentUser** inside the **updated()** hook which of course results in the **currentUser** to be reset to the **firstUser** every single time the select is changed. I have moved the **currentUser** initialisation to the root **App** instead.

Checkbox binding proved a bit troublesome to work with in Vue. I solved it in the end by using **v-model** to bind the checkbox's checked status to the store instead of using **:value**. I then did a computed property for the binding with a getter and a setter; turns out that by doing it this way, the binding works immediately without me needing to establish a **@change** listener in the checkbox itself. Pretty cool.

### Thursday 18 July 2019 2:00pm

I love Vue! At first I felt like I was cheating on React but now I really like using Vue. I like how structured it is. With React, everything is just Javascript so maintainability is really up to the developer on how neat a code they write. With Vue, it's so easy to read a component and see what it's doing because of how strict it is with the various hooks that it enforces.

### Thursday 18 July 2019 11:21pm

Today I worked on editing and deleting todo items. I now understand how how mutations and actions work and how they differ. A single state can have just a single mutation whereas actions that are meant to modify a particular state will use that one mutation for that purpose

### Friday 19 July 2019 7:14am

I replaced the Todo input text field with a textarea. My plan is to introduce an auto-expand functionality to the input so that it can have a multiline property and can grow depending on the length of each todo item.

### Sunday 21 July 2019 1:42am

Finally got the auto-expand functionality of the **textarea** to work. In the end I employed a bit of a hack but I think it provides the best UX overall.

### Sunday 21 July 2019 7:52

I added a new **addTodo** entry at the bottom of the page.

### Monday 22 July 2019 11:14pm

I wrote 5 really basic tests to mainly do snapshot testing on some of the pure view components, and tested that **Todos** and **UserSelector** displays the correct number of items based on a mock store.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
