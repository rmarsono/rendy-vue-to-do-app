export const findTodoIndex = (todos, id) => {
  const index = todos.findIndex(todo => {
    return todo.id === id;
  });
  return index;
};

export const randomId = () => {
  return Math.round(Date.now() * (Math.random() * 100000));
};

export function isEmptyObject(obj) {
  for (var prop in obj) {
    return false;
  }
  return true;
}
