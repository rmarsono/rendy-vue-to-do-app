import Vue from "vue";
import Vuex from "vuex";

import { findTodoIndex, randomId } from "./helpers";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todos: [],
    users: [],
    currentUser: 0,
    isLoading: false
  },
  getters: {
    todos(state) {
      return state.todos;
    },
    users(state) {
      return state.users;
    },
    currentUser(state) {
      return state.currentUser;
    },
    isLoading(state) {
      return state.isLoading;
    }
  },
  mutations: {
    setTodos(state, todos) {
      state.todos = [...todos];
    },
    setUsers(state, users) {
      state.users = [...users];
    },
    setCurrentUser(state, currentUser) {
      state.currentUser = currentUser;
    },
    setIsLoading(state, isLoading) {
      state.isLoading = isLoading;
    }
  },
  actions: {
    setTodos(context, todos) {
      context.commit("setTodos", todos);
    },
    setUsers(context, users) {
      context.commit("setUsers", users);
    },
    setCurrentUser(context, currentUser) {
      context.commit("setCurrentUser", currentUser);
    },
    updateTitle(context, payload) {
      const { todos } = { ...context.state };
      const { id, newTitle } = payload;
      todos[findTodoIndex(todos, id)].title = newTitle;
      context.commit("setTodos", todos);
    },
    completeTodo(context, payload) {
      const { todos } = { ...context.state };
      const { id, isCompleted } = payload;
      todos[findTodoIndex(todos, id)].completed = isCompleted;
      context.commit("setTodos", todos);
    },
    removeTodo(context, id) {
      const { todos } = { ...context.state };
      todos.splice(findTodoIndex(todos, id), 1);
      context.commit("setTodos", todos);
    },
    addTodo(context, newTitle) {
      const { currentUser } = { ...context.state };
      let { todos } = { ...context.state };
      todos.push({
        userId: parseInt(currentUser),
        id: randomId(),
        title: newTitle,
        completed: false
      });
      context.commit("setTodos", todos);
    },
    setIsLoading(context, isLoading) {
      context.commit("setIsLoading", isLoading);
    }
  }
});
